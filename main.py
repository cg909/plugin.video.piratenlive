# -*- encoding: utf-8 -*-
# Author: Christoph Grenz
# License: GPLv3 https://www.gnu.org/copyleft/gpl.html

from __future__ import division, absolute_import, print_function, unicode_literals

import sys
import xbmcgui
import xbmcplugin

try:
	from urllib.parse import parse_qsl, urlencode, urlparse
	from urllib.request import urlopen
	from html.parser import HTMLParser
except ImportError:
	from urllib import urlencode
	from urlparse import parse_qsl, urlparse
	from urllib2 import urlopen
	from HTMLParser import HTMLParser
	bytes = str
	str = unicode

_URL = sys.argv[0] #: Plugin url in plugin:// notation.
_HANDLE = int(sys.argv[1]) #: Plugin handle as an integer number.

MAINURL = 'https://piraten.live/'
VIDEO_URL_TEMPLATE = 'https://piraten.live/hls/{0}.m3u8'
AUDIO_URL_TEMPLATE = 'https://piraten.live/hls/{0}_audio/index.m3u8'

class IndexParser(HTMLParser):
	''' Parser for stream index of piraten.live '''
	def __init__(self):
		self._result = []
		self._tmp = None
		self._in_name = None
		HTMLParser.__init__(self)
	
	def handle_starttag(self, tag, attrs):
		attrs = dict(attrs)
		itemscope = 'itemscope' in attrs
		itemtype = attrs.get('itemtype')
		itemprop = attrs.get('itemprop')
		if itemscope and itemtype == "http://schema.org/VideoObject":
			self._tmp = {'tag': tag, 'name': '', 'info': ''}
		elif self._tmp and itemprop == 'name':
			self._in_name = tag
		elif self._tmp and itemprop == 'url':
			self._tmp['url'] = attrs['href']

	def handle_endtag(self, tag):
		if self._tmp:
			if tag == self._in_name:
				self._tmp['name'] = self._tmp['name'].strip()
				self._in_name = None
			elif tag == self._tmp['tag']:
				del self._tmp['tag']
				self._tmp['info'] = self._tmp['info'].strip()
				self._result.append(self._tmp)
				self._tmp = None

	def handle_data(self, data):
		if self._tmp:
			if self._in_name:
				self._tmp['name'] += data
			elif data.strip():
				self._tmp['info'] += data
	
	def get_result(self):
		return self._result

def get_url(**kwargs):
    '''
    Create a URL for calling the plugin recursively from the given set of keyword arguments.

    :param kwargs: "argument=value" pairs
    :type kwargs: dict
    :return: plugin call URL
    :rtype: str
    '''
    return '{0}?{1}'.format(_URL, urlencode(kwargs))

def get_videos():
	'''
		Get the list of streams.

		:return: the list of videos in the category
		:rtype: list
	'''
	response = urlopen(MAINURL)
	parser = IndexParser()
	data = response.read().decode('utf-8')
	parser.feed(data)
	parser.close()
	return [item for item in parser.get_result() if item.get('url') and item.get('name')]

def list_videos():
	'''
		Create the list of playable videos in the Kodi interface.
	'''
	# Set plugin category. It is displayed in some skins as the name
	# of the current section.
	xbmcplugin.setPluginCategory(_HANDLE, 'Piraten-Live')
	# Set plugin content. It allows Kodi to select appropriate views
	# for this type of content.
	xbmcplugin.setContent(_HANDLE, 'videos')
	# Iterate through videos.
	for video in get_videos():
		# Create a list item with a text label and a thumbnail image.
		list_item = xbmcgui.ListItem(label=video['name'])
		# Set additional info for the list item.
		# 'mediatype' is needed for skin to display info for this ListItem correctly.
		list_item.setInfo('video', {'title': video['name'],
									'genre': 'Live',
									'plot': video['info'],
									'tagline': video['info'],
									'mediatype': 'video'})
		list_item.setArt({'thumb': 'https://piraten.live/images/logo_16x9.png'})
		# Set 'IsPlayable' property to 'true'.
		# This is mandatory for playable items!
		list_item.setProperty('IsPlayable', 'true')
		
		# Create a URL for a plugin recursive call.
		parts = urlparse(video['url'])
		video_id = parts.path.strip('/')
		url = get_url(action='play', video=video_id)
		# Add the list item to a virtual Kodi folder.
		is_folder = False
		# Add our item to the Kodi virtual folder listing.
		xbmcplugin.addDirectoryItem(_HANDLE, url, list_item, is_folder)
	# Add a sort method for the virtual folder items (alphabetically, ignore articles)
	xbmcplugin.addSortMethod(_HANDLE, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
	# Finish creating a virtual folder.
	xbmcplugin.endOfDirectory(_HANDLE)

def play_video(videoid):
	'''
		Play a video stream

		:type videoid: str
	'''
	
	path = VIDEO_URL_TEMPLATE.format(videoid)
	
	# Create a playable item with a path to play.
	play_item = xbmcgui.ListItem(path=path)
	play_item.setInfo('video', {'mediatype': 'video'})
	# Pass the item to the Kodi player.
	xbmcplugin.setResolvedUrl(_HANDLE, True, listitem=play_item)

def play_audio(videoid):
	'''
		Play an audio-only stream

		:type videoid: str
	'''
	
	path = AUDIO_URL_TEMPLATE.format(videoid)
	
	# Create a playable item with a path to play.
	play_item = xbmcgui.ListItem(path=path)
	play_item.setInfo('music', {'mediatype': 'music'})
	# Pass the item to the Kodi player.
	xbmcplugin.setResolvedUrl(_HANDLE, True, listitem=play_item)


def router(paramstring):
	'''
		Router function that calls other functions
		depending on the provided paramstring

		:param paramstring: URL encoded plugin paramstring
		:type paramstring: str
	'''
	# Parse a URL-encoded paramstring to the dictionary of
	# {<parameter>: <value>} elements
	params = dict(parse_qsl(paramstring))
	# Check the parameters passed to the plugin
	if params:
		if params['action'] == 'play':
			# Play a video from a provided URL.
			play_video(params['video'])
		elif params['action'] == 'audio':
			# Play a video from a provided URL.
			play_audio(params['video'])
		else:
			# If the provided paramstring does not contain a supported action
			# we raise an exception. This helps to catch coding errors,
			# e.g. typos in action names.
			raise ValueError('Invalid paramstring: {0}!'.format(paramstring))
	else:
		# If the plugin is called from Kodi UI without any parameters,
		# display the list of video categories
		list_videos()

if __name__ == '__main__':
	# Call the router function and pass the plugin call parameters to it.
	# We use string slicing to trim the leading '?' from the plugin call paramstring
	router(sys.argv[2][1:])
