# Piraten.Live Plugin for Kodi 18 (plugin.video.piratenlive)

This is a simple plugin for [Kodi](http://kodi.tv) mediacenter providing access
to all active live streams on [piraten.live](https://piraten.live). It mainly
targets Piratenpartei members and associates.

This plugin is not officially commisioned/supported by piraten.live.

## Prerequisites ##

- Kodi 18
- Python 2.6+ (should be included in your Kodi 18 installation)

## License ##

[GPL v.3](http://www.gnu.org/copyleft/gpl.html)
